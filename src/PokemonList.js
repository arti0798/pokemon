import React from 'react'

function PokemonList({ pokemon }) {
  return (
    <div>
        { pokemon.map(p => (
            <h4 key={p}>{p}</h4>
        ))}
    </div>
  )
}

export default PokemonList
